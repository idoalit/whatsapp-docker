import whatsappweb from "whatsapp-web.js";
import {handleMessage} from "./modules/index.js"

const { Client, LocalAuth } = whatsappweb;
import qrcode from "qrcode-terminal";


// Create whatsapp client instance
const client = new Client({
    authStrategy: new LocalAuth(),
    puppeteer: { headless: true, args: [ '--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage', '--disable-accelerated-2d-canvas', '--no-first-run', '--no-zygote', '--single-process', '--disable-gpu', ], },
});

client.initialize();

client.on('qr', (qr) => {
    // Generate and scan this code with your phone
    qrcode.generate(qr, { small: true });
});

client.on("authenticated", () => {
    console.log("Authentication complete");
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', msg => {
    handleMessage(msg)
});

client.initialize();